package com.crio.jukebox.appConfig;

import com.crio.jukebox.commands.CommandInvoker;
import com.crio.jukebox.commands.CreatePlayListCommand;
import com.crio.jukebox.commands.CreateUserCommand;
import com.crio.jukebox.commands.DeletePlayListCommand;
import com.crio.jukebox.commands.LoadSongCommands;
import com.crio.jukebox.commands.ModifyPlayListCommand;
import com.crio.jukebox.commands.PlayPlayListCommand;
import com.crio.jukebox.commands.PlaySongCommand;
import com.crio.jukebox.repositories.IPlayListRepository;
import com.crio.jukebox.repositories.ISongRepository;
import com.crio.jukebox.repositories.IUserRepository;
import com.crio.jukebox.repositories.PlayListRepository;
import com.crio.jukebox.repositories.SongRepository;
import com.crio.jukebox.repositories.UserRepository;
import com.crio.jukebox.services.IPlayListSevvice;
import com.crio.jukebox.services.ISongService;
import com.crio.jukebox.services.IUserService;
import com.crio.jukebox.services.PlayListService;
import com.crio.jukebox.services.SongService;
import com.crio.jukebox.services.UserService;

public class ApplicationConfig {
    private final ISongRepository sonngrepository=new SongRepository();
    private final IUserRepository userRepository=new UserRepository();
    private final CommandInvoker commandInvoker = new CommandInvoker();
    private final IPlayListRepository iPlayListRepository=new PlayListRepository();
    private final ISongService iSongService=new SongService(sonngrepository);
    
    private final LoadSongCommands loadSongCommands=new LoadSongCommands(iSongService);
    

    
    private final IUserService iUserService=new UserService(userRepository,iPlayListRepository);
    private final CreateUserCommand createUserCommand=new CreateUserCommand(iUserService);

    
    private final IPlayListSevvice iPlayListSevvice=new PlayListService(iPlayListRepository, sonngrepository);
    private final CreatePlayListCommand createPlayListCommand=new CreatePlayListCommand(iPlayListSevvice);


    private final DeletePlayListCommand deletePlayListCommand=new DeletePlayListCommand(iPlayListSevvice);
    private final PlayPlayListCommand playPlayListCommand=new PlayPlayListCommand(iUserService, iSongService);

    private final ModifyPlayListCommand modifyPlayListCommand=new ModifyPlayListCommand(iPlayListSevvice, iSongService);
    private final PlaySongCommand playSongCommand=new PlaySongCommand(iUserService, iSongService);

    public CommandInvoker getCommandInvoker(){
        commandInvoker.register("LOAD-DATA",loadSongCommands);
        commandInvoker.register("CREATE-USER",createUserCommand);
        commandInvoker.register("CREATE-PLAYLIST",createPlayListCommand);
        commandInvoker.register("DELETE-PLAYLIST",deletePlayListCommand);
        commandInvoker.register("MODIFY-PLAYLIST",modifyPlayListCommand);
        commandInvoker.register("PLAY-PLAYLIST",playPlayListCommand);
        commandInvoker.register("PLAY-SONG",playSongCommand);
        return commandInvoker;
    }
}
