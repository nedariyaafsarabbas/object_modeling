package com.crio.jukebox.commands;

import java.util.ArrayList;
import java.util.List;
import com.crio.jukebox.entities.Playlist;
import com.crio.jukebox.services.IPlayListSevvice;

public class CreatePlayListCommand implements ICommand{

    private final IPlayListSevvice iPlayListSevvice;
    // private final ISongService iSongService;


    // public CreatePlayListCommand(IPlayListSevvice iPlayListSevvice,ISongService iSongService) {
    //     this.iPlayListSevvice = iPlayListSevvice;
    //     this.iSongService=iSongService;
    // }

        public CreatePlayListCommand(IPlayListSevvice iPlayListSevvice){
            this.iPlayListSevvice=iPlayListSevvice;
        }

        
    // TODO: CRIO_TASK_MODULE_CONTROLLER
    // Execute create method of IUserService and print the result.
    // Look for the unit tests to see the expected output.
    // Sample Input Token List:- ["CREATE_QUESTION","Ross"]

    @Override
    public void execute(List<String> tokens) {
        // TODO Auto-generated method stub
        try {
            String userId=tokens.get(1);
            String playListName=tokens.get(2);
            List<String> songIds=new ArrayList<>();
            // List<Song> songs=new ArrayList<>();
            for(int i=3;i<tokens.size();i++){
                songIds.add(tokens.get(i));
                // Song song=iSongService.findById(tokens.get(i));
                // songs.add(song);
            }
            Playlist playlist=iPlayListSevvice.create(new Playlist(userId, playListName, songIds));
            System.out.println("Playlist ID"+" - "+playlist.getId());
        } catch (Exception e) {
            //TODO: handle exception
            System.out.println(e.getMessage());
        }

        
    }
    
}
