package com.crio.jukebox.commands;

import java.util.List;
import com.crio.jukebox.services.IPlayListSevvice;

public class DeletePlayListCommand implements ICommand{

    private final IPlayListSevvice iPlayListSevvice;

    

    public DeletePlayListCommand(IPlayListSevvice iPlayListSevvice) {
        this.iPlayListSevvice = iPlayListSevvice;
    }



    @Override
    public void execute(List<String> tokens) {
        // TODO Auto-generated method stub
        try {
            iPlayListSevvice.deleteById(tokens.get(2));
            System.out.println("Delete Successful");
        } catch (Exception e) {
            //TODO: handle exception
            System.out.println(e.getMessage());
        }
        
    }
    
}
