package com.crio.jukebox.commands;

import java.io.FileReader;
import java.util.List;
import com.crio.jukebox.entities.Song;
import com.crio.jukebox.services.ISongService;
import com.opencsv.CSVReader;

public class LoadSongCommands implements ICommand {
    private final ISongService  iSongService;
    
    public LoadSongCommands(ISongService iSongService){
        this.iSongService=iSongService;
    }

    @Override
    public void execute(List<String> tokens) {
        // TODO Auto-generated method stub
        try {
            CSVReader  reader =new CSVReader(new FileReader(tokens.get(1)));
            String[] lineInArray;
            while ((lineInArray = reader.readNext()) != null) {
                String artists=lineInArray[4];
                artists=artists.replaceAll("#", ",");
                Song song=new Song(lineInArray[0], lineInArray[1], lineInArray[2], artists); 
                iSongService.addSong(song);
            }
            System.out.println("Songs Loaded successfully");
        } catch (Exception e) {
            //TODO: handle exception
            System.out.println(e);
        }
    }
    
}
