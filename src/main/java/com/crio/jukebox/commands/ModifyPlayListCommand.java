package com.crio.jukebox.commands;

import java.util.ArrayList;
import java.util.List;
import com.crio.jukebox.entities.Playlist;
import com.crio.jukebox.services.IPlayListSevvice;
import com.crio.jukebox.services.ISongService;

public class ModifyPlayListCommand implements ICommand {

    private final IPlayListSevvice iPlayListSevvice;
    private final ISongService iSongService;


    public ModifyPlayListCommand(IPlayListSevvice iPlayListSevvice, ISongService iSongService) {
        this.iPlayListSevvice = iPlayListSevvice;
        this.iSongService = iSongService;
    }


    @Override
    public void execute(List<String> tokens) {
        // TODO Auto-generated method stub
        String opration = tokens.get(1);
        String userId = tokens.get(2);
        String playListId = tokens.get(3);
        List<String> songIds = new ArrayList<>();
        for (int i = 4; i < tokens.size(); i++) {
            songIds.add(tokens.get(i));
        }
        if(opration.equalsIgnoreCase("ADD-SONG")){
            try {
                Playlist playlist=iPlayListSevvice.addSongInPlayList(playListId, userId, songIds);
                System.out.println("Playlist ID - "+playlist.getId());
                System.out.println("Playlist Name - "+playlist.getPlayListName());
                List<String> songs2=playlist.getSongList();
                System.out.print("Song IDs - ");
                for(String s:songs2){
                    System.out.print(s);
                    if(!songs2.get(songs2.size()-1).equals(s)){
                        System.out.print(" ");
                    }
                }
                System.out.println();
            } catch (Exception e) {
                //TODO: handle exception
                System.out.println(e.getMessage());
            }
        }
        else{
            try {
                Playlist playlist=iPlayListSevvice.deleteSongfromPlayList(playListId, userId, songIds);
                System.out.println("Playlist ID - "+playlist.getId());
                System.out.println("Playlist Name - "+playlist.getPlayListName());
                List<String> songs2=playlist.getSongList();
                System.out.print("Song IDs - ");
                for(String s:songs2){
                    System.out.print(s);
                    if(!songs2.get(songs2.size()-1).equals(s)){
                        System.out.print(" ");
                    }
                }
                System.out.println();
            } catch (Exception e) {
                //TODO: handle exception
                System.out.println(e.getMessage());
            }
        }
    }
}
