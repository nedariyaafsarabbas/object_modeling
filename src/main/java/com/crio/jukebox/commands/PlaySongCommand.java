package com.crio.jukebox.commands;

import java.util.List;
import com.crio.jukebox.entities.Song;
import com.crio.jukebox.entities.User;
import com.crio.jukebox.services.ISongService;
import com.crio.jukebox.services.IUserService;

public class PlaySongCommand implements ICommand {

    private final IUserService userService;
    private final ISongService iSongService;

    public PlaySongCommand(IUserService userService, ISongService iSongService) {
        this.userService = userService;
        this.iSongService = iSongService;
    }

    @Override
    public void execute(List<String> tokens) {
        // TODO Auto-generated method stub
        String action = tokens.get(2);
        try {
            if (action.equals("BACK")) {
                User user = userService.switchToBackSong(tokens.get(1));
                Song song = iSongService.findById(user.getSongId());
                System.out.println("Current Song Playing");
                System.out.println("Song - " + song.getSongName());
                System.out.println("Album - " + song.getAlbumName());
                System.out.println("Artists - " + song.getAlbumartist());

            } else if (action.equals("NEXT")) {
                User user = userService.switchToNextSong(tokens.get(1));
                Song song = iSongService.findById(user.getSongId());
                System.out.println("Current Song Playing");
                System.out.println("Song - " + song.getSongName());
                System.out.println("Album - " + song.getAlbumName());
                System.out.println("Artists - " + song.getAlbumartist());

            } else {
                User user = userService.switchToPreferredSong(tokens.get(1), tokens.get(2));
                Song song = iSongService.findById(user.getSongId());
                System.out.println("Current Song Playing");
                System.out.println("Song - " + song.getSongName());
                System.out.println("Album - " + song.getAlbumName());
                System.out.println("Artists - " + song.getAlbumartist());

            }
        } catch (Exception e) {
            // TODO: handle exception
            System.out.println(e.getMessage());
        }

    }
}
