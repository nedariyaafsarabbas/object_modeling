package com.crio.jukebox.entities;

import java.util.List;

public class Playlist extends BaseEntity {
    
    private String userId;
    private String playListName;
    private List<String> songList;
    public Playlist(String userId, String playListName, List<String> songList) {
        this.userId = userId;
        this.playListName = playListName;
        this.songList = songList;
    }
    public Playlist(String id,String userId, String playListName, List<String> songList) {
        this.id=id;
        this.userId = userId;
        this.playListName = playListName;
        this.songList = songList;
    }
    public String getUserId() {
        return userId;
    }
    public void setUserId(String userId) {
        this.userId = userId;
    }
    public String getPlayListName() {
        return playListName;
    }
    public void setPlayListName(String playListName) {
        this.playListName = playListName;
    }
    public List<String> getSongList() {
        return songList;
    }
    public void setSongList(List<String> songList) {
        this.songList = songList;
    }

    


}
