package com.crio.jukebox.entities;

public class Song extends BaseEntity {

    private String songName;
    private String genre;
    private String albumName;
    private String albumartist;

    public String getAlbumartist() {
        return albumartist;
    }
    public void setAlbumartist(String albumartist) {
        this.albumartist = albumartist;
    }
    public Song(String id,String songName, String genre, String albumName, String albumartist) {
        this.id=id;
        this.songName = songName;
        this.genre = genre;
        this.albumName = albumName;
        this.albumartist=albumartist;
    }
    public Song(String songName, String genre, String albumName, String albumartist) {
        this.songName = songName;
        this.genre = genre;
        this.albumName = albumName;
        this.albumartist=albumartist;
    }
    public String getSongName() {
        return songName;
    }
    public void setSongName(String songName) {
        this.songName = songName;
    }
    public String getGenre() {
        return genre;
    }
    public void setGenre(String genre) {
        this.genre = genre;
    }
    public String getAlbumName() {
        return albumName;
    }
    public void setAlbumName(String albumName) {
        this.albumName = albumName;
    }
    
    
    
}
