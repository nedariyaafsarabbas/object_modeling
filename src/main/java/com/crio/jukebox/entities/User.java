package com.crio.jukebox.entities;

public class User extends BaseEntity {
   
    private final String name;
    private String playListId;
    public String getPlayListId() {
        return playListId;
    }

    public void setPlayListId(String playListId) {
        this.playListId = playListId;
    }

    public String getSongId() {
        return songId;
    }

    public void setSongId(String songId) {
        this.songId = songId;
    }

    private String songId;

    public User(String name, String playListId, String songId) {
        this.name = name;
        this.playListId = playListId;
        this.songId = songId;
    }

    public User(String name) {
        this.name = name;
    }

    public User(String id,String name) {
        this.id=id;
        this.name = name;
    }

    public String getName() {
        return this.name;
    }    
    
}

