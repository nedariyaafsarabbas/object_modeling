package com.crio.jukebox.repositories;

import java.util.List;
import com.crio.jukebox.entities.Playlist;

public interface IPlayListRepository extends CRUDRepository<Playlist,String>{
    public Playlist addSongInPlayList(String playListId,String userId,List<String> songs);
    public Playlist deleteSongfromPlayList(String playListId,String userId,List<String> songs);
    
}
