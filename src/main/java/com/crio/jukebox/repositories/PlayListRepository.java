package com.crio.jukebox.repositories;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import com.crio.jukebox.entities.Playlist;
import com.crio.jukebox.entities.Song;

public class PlayListRepository implements IPlayListRepository {


    private final Map<String, Playlist> playLists;
    private Integer autoIncrement = 0;

    public PlayListRepository(Map<String, Playlist> playLists, Integer autoIncrement) {
        this.playLists = playLists;
        this.autoIncrement = autoIncrement;
    }

    public PlayListRepository() {
        this.playLists=new HashMap<>();
    }

    @Override
    public Playlist save(Playlist entity) {
        // TODO Auto-generated method stub
        if(entity.getId() == null){
            autoIncrement++;
            Playlist song=new Playlist(Integer.toString(autoIncrement), entity.getUserId(), entity.getPlayListName(), entity.getSongList());
            playLists.put(song.getId(), song);
            return song;
        }
        playLists.put(entity.getId(), entity);
            return entity;
    }

    @Override
    public List<Playlist> findAll() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Optional<Playlist> findById(String id) {
        // TODO Auto-generated method stub
        return Optional.ofNullable(playLists.get(id));
    }

    @Override
    public boolean existsById(String id) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public void delete(Playlist entity) {
        // TODO Auto-generated method stub
        playLists.remove(entity.getId(),entity);
        
    }

    @Override
    public void deleteById(String id) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public long count() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public Playlist addSongInPlayList(String playListId, String userId, List<String> songs) {
        // TODO Auto-generated method stub
        playLists.get(playListId).getSongList().addAll(songs);
        return playLists.get(playListId);
    }

    @Override
    public Playlist deleteSongfromPlayList(String playListId, String userId, List<String> songs) {
        // TODO Auto-generated method stub
        playLists.get(playListId).getSongList().removeAll(songs);
        return playLists.get(playListId);
    }

    
}
