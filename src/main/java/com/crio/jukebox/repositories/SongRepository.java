package com.crio.jukebox.repositories;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import com.crio.jukebox.entities.Song;

public class SongRepository implements  ISongRepository {


    private final Map<String, Song> songList;
    private Integer autoIncrement = 0;


    public SongRepository(Map<String, Song> songList, Integer autoIncrement) {
        this.songList = songList;
        this.autoIncrement = autoIncrement;
    }

    public SongRepository() {
        songList = new HashMap<>();
    }

    @Override
    public Song save(Song entity) {
        // TODO Auto-generated method stub
        if(entity.getId() == null){
            autoIncrement++;
            Song song=new Song(Integer.toString(autoIncrement), entity.getSongName(), entity.getGenre(), entity.getAlbumName(),entity.getAlbumartist());
            songList.put(song.getId(), song);
            return song;

        }
        songList.put(entity.getId(), entity);
            return entity;
    }

    @Override
    public List<Song> findAll() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Optional<Song> findById(String id) {
        // TODO Auto-generated method stub
        return Optional.ofNullable(songList.get(id));
    }

    @Override
    public boolean existsById(String id) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public void delete(Song entity) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void deleteById(String id) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public long count() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public Optional<Song> findByName(String name) {
        // TODO Auto-generated method stub
        return null;
    }
    
}
