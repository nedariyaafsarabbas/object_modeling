package com.crio.jukebox.services;

import java.util.List;
import com.crio.jukebox.entities.Playlist;
import com.crio.jukebox.exceptions.PlayListNotFoundException;
import com.crio.jukebox.exceptions.SongNotFoundException;

public interface IPlayListSevvice {
    public Playlist create(Playlist playlist);
    public Playlist findById(String id) throws PlayListNotFoundException;
    public void deleteById(String id) throws PlayListNotFoundException;
    public Playlist addSongInPlayList(String playListId,String userId,List<String> songs) throws SongNotFoundException;
    public Playlist deleteSongfromPlayList(String playListId,String userId,List<String> songs) throws SongNotFoundException;
}
