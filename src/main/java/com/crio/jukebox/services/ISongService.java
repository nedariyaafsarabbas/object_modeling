package com.crio.jukebox.services;

import com.crio.jukebox.entities.Song;
import com.crio.jukebox.exceptions.SongNotFoundException;

public interface ISongService {
    public Song addSong(Song song);
    public Song findById(String id) throws SongNotFoundException;
}
