package com.crio.jukebox.services;

import com.crio.jukebox.entities.User;
import com.crio.jukebox.exceptions.SongNotFoundException;
import com.crio.jukebox.exceptions.UserNotFoundException;



public interface IUserService {
    public User create(String name);
    public User findById(String id) throws UserNotFoundException;
    public User playThePlayList(String userId,String playListId) throws SongNotFoundException;
    public User switchToBackSong(String userId);
    public User switchToNextSong(String userId);
    public User switchToPreferredSong(String userId,String songId);
}
