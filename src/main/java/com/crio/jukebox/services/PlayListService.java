package com.crio.jukebox.services;

import java.util.List;
import com.crio.jukebox.entities.Playlist;
import com.crio.jukebox.exceptions.PlayListNotFoundException;
import com.crio.jukebox.exceptions.SongNotFoundException;
import com.crio.jukebox.repositories.IPlayListRepository;
import com.crio.jukebox.repositories.ISongRepository;

public class PlayListService implements IPlayListSevvice {

    private IPlayListRepository playListRepository;
    private ISongRepository songRepository;

    
    public PlayListService(IPlayListRepository iPlayListRepository, ISongRepository sonngrepository) {
        this.playListRepository = iPlayListRepository;
        this.songRepository = sonngrepository;
    }

    @Override
    public Playlist create(Playlist playlist) throws SongNotFoundException {
        // TODO Auto-generated method stub
        for(String p:playlist.getSongList()){
            songRepository.findById(p).orElseThrow(() -> new SongNotFoundException("Some Requested Songs Not Available. Please try again."));
        }
        return playListRepository.save(playlist);
    }

    @Override
    public Playlist findById(String id) throws PlayListNotFoundException {
        // TODO Auto-generated method stub
        Playlist playlist=playListRepository.findById(id)
                    .orElseThrow(() -> new PlayListNotFoundException("Playlist Not Found"));
        return playlist;
    }

    @Override
    public void deleteById(String id) throws PlayListNotFoundException {
        // TODO Auto-generated method stub
        Playlist playlist=playListRepository.findById(id)
                    .orElseThrow(() -> new PlayListNotFoundException("Playlist Not Found"));
        playListRepository.delete(playlist);
    }

    @Override
    public Playlist addSongInPlayList(String playListId, String userId,
            List<String> songs) {
        // TODO Auto-generated method stub
        for(String p:songs){
            songRepository.findById(p).orElseThrow(() -> new SongNotFoundException("Some Requested Songs Not Available. Please try again."));
        }
        Playlist playlist=playListRepository.addSongInPlayList(playListId, userId, songs);
        return playlist;
    }

    @Override
    public Playlist deleteSongfromPlayList(String playListId, String userId,
            List<String> songs) {
        // TODO Auto-generated method stub
        Playlist  playlist=playListRepository.findById(playListId).orElseThrow(() -> new PlayListNotFoundException("Playlist Not Found"));
        // checking song in playlist
        List<String> songInPaylList=playlist.getSongList();
        boolean status=songs.stream().anyMatch(e -> songInPaylList.contains(e));
        if(!status){
            throw new SongNotFoundException("Some Requested Songs for Deletion are not present in the playlist. Please try again.");
        }
        return playListRepository.deleteSongfromPlayList(playListId, userId, songs);   
    }
   
}
