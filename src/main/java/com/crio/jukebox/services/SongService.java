package com.crio.jukebox.services;

import com.crio.jukebox.entities.Song;
import com.crio.jukebox.exceptions.SongNotFoundException;
import com.crio.jukebox.repositories.ISongRepository;
import com.crio.jukebox.repositories.SongRepository;

public class SongService implements ISongService {

    private ISongRepository sonngrepository;
    

    public SongService(ISongRepository sonngrepository2) {
        this.sonngrepository = sonngrepository2;
    }

    @Override
    public Song addSong(Song song) {
        // TODO Auto-generated method stub

        return sonngrepository.save(song);
    }

    @Override
    public Song findById(String id) throws SongNotFoundException {
        // TODO Auto-generated method stub
        Song song=sonngrepository.findById(id).orElseThrow(() -> new SongNotFoundException("Some Requested Songs Not Available. Please try again."));
        return song;
    }
    
}
