package com.crio.jukebox.services;


import java.util.List;
import com.crio.jukebox.entities.Playlist;
import com.crio.jukebox.entities.User;
import com.crio.jukebox.exceptions.PlayListNotFoundException;
import com.crio.jukebox.exceptions.SongNotFoundException;
import com.crio.jukebox.exceptions.UserNotFoundException;
import com.crio.jukebox.repositories.IPlayListRepository;
import com.crio.jukebox.repositories.IUserRepository;



public class UserService implements IUserService {

    private final IUserRepository userRepository;
    private IPlayListRepository playListRepository;



    public UserService(IUserRepository userRepository, IPlayListRepository playListRepository) {
        this.userRepository = userRepository;
        this.playListRepository = playListRepository;
    }

    public UserService(IUserRepository userRepository) {
        this.userRepository = userRepository;
    }

    // TODO: CRIO_TASK_MODULE_SERVICES
    // Create and store User into the repository.
    @Override
    public User create(String name) {
        User user = new User("1", name);
        userRepository.save(user);
        return user;
    }

    @Override
    public User findById(String id) throws UserNotFoundException {
        // TODO Auto-generated method stub
        User user = userRepository.findById(id)
                .orElseThrow(() -> new UserNotFoundException("User Not Found"));
        return user;
    }

    @Override
    public User playThePlayList(String userId, String playListId) throws SongNotFoundException {
        // TODO Auto-generated method stub
        User user = userRepository.findById(userId)
                .orElseThrow(() -> new UserNotFoundException("User Not Found"));
        Playlist playlist = playListRepository.findById(playListId)
                .orElseThrow(() -> new SongNotFoundException("PlayList Not Found"));
        user.setPlayListId(playlist.getId());
        if (playlist.getSongList().isEmpty()) {
            throw new SongNotFoundException("Playlist is empty.");
        }
        user.setSongId(playlist.getSongList().get(0));
        return user;
    }

    @Override
    public User switchToBackSong(String userId) {
        // TODO Auto-generated method stub

        User user = userRepository.findById(userId)
                .orElseThrow(() -> new UserNotFoundException("User not found"));
        String currentSong = user.getSongId();
        String currentPlayListId = user.getPlayListId();
        int currentPosi=findCurrentSongPosition(currentSong, currentPlayListId);
        List<String> songInPlayList=playListRepository.findById(user.getPlayListId())
        .orElseThrow(() -> new PlayListNotFoundException("Playlist Not found")).getSongList();
        if(currentPosi==0){
            user.setSongId(songInPlayList.get(songInPlayList.size()-1));

        }
        else{
            user.setSongId(songInPlayList.get(currentPosi-1));
        }
        return user;
    }


    private int findCurrentSongPosition(String songId, String playListId) {
        Playlist playlist = playListRepository.findById(playListId)
                .orElseThrow(() -> new PlayListNotFoundException("Playlist Not found"));
        List<String> songList = playlist.getSongList();
        int songPosi = -1;
        for (int i = 0; i < songList.size(); i++) {
            if (songList.get(i).equals(songId)) {
                songPosi = i;
                break;
            }
        }
        return songPosi;
    }

    @Override
    public User switchToNextSong(String userId) {
        // TODO Auto-generated method stub
        User user = userRepository.findById(userId)
                .orElseThrow(() -> new UserNotFoundException("User not found"));
        String currentSong = user.getSongId();
        String currentPlayListId = user.getPlayListId();
        int currentPosi=findCurrentSongPosition(currentSong, currentPlayListId);
        List<String> songInPlayList=playListRepository.findById(user.getPlayListId())
        .orElseThrow(() -> new PlayListNotFoundException("Playlist Not found")).getSongList();
        if(currentPosi==songInPlayList.size()-1){
            user.setSongId(songInPlayList.get(0));

        }
        else{
            user.setSongId(songInPlayList.get(currentPosi+1));
        }
        return user;
    }

    @Override
    public User switchToPreferredSong(String userId, String songId) {
        // TODO Auto-generated method stub
        User user = userRepository.findById(userId)
                .orElseThrow(() -> new UserNotFoundException("User not found"));
        String currentPlayListId = user.getPlayListId();
        int currentPosi=findCurrentSongPosition(songId, currentPlayListId);
        List<String> songInPlayList=playListRepository.findById(user.getPlayListId())
        .orElseThrow(() -> new PlayListNotFoundException("Playlist Not found")).getSongList();
        if(currentPosi==-1){
            throw new SongNotFoundException("Given song id is not a part of the active playlist");

        }
        else{
            user.setSongId(songInPlayList.get(currentPosi));

        }
        return user;
    }


}
