package com.crio.jukebox.commands;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.List;
import com.crio.jukebox.services.ISongService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@DisplayName("LoadSongCommandsTest")
@ExtendWith(MockitoExtension.class)
public class LoadSongCommandsTest {


    private final PrintStream standardOut = System.out;
    private final ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();
    
    @Mock
    ISongService songService;

    @InjectMocks
    LoadSongCommands loadSongCommands;

    @BeforeEach
    public void setUp() {
        System.setOut(new PrintStream(outputStreamCaptor));
    }

    @Test
    public void loadCsvSong(){
        String expectedOutput="Songs Loaded successfully";
        loadSongCommands.execute(List.of("LOAD-DATA", "songs.csv"));
        Assertions.assertEquals(expectedOutput, outputStreamCaptor.toString().trim());   
    }
    
}
